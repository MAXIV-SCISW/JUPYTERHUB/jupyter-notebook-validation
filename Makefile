###############################################################################
# This mmakefile is made to (hopefully) simplify the running of some standard
# Jupyter notebook validation routines.
###############################################################################

# Makefile stuff
.DEFAULT_GOAL := help
SHELL := /bin/bash
.ONESHELL:
THIS_FILE := $(lastword $(MAKEFILE_LIST))

# Set the umask so that everyone has atleast read access to results
UMASK := umask 002

# Colors
LIGHTPURPLE := \033[1;35m
GREEN := \033[32m
CYAN := \033[36m
BLUE := \033[34m
RED := \033[31m
NC := \033[0m

# Separator between output, 80 characters wide
define print_separator
	printf "$1"; printf "%0.s*" {1..80}; printf "$(NC)\n"
endef
print_line_green = $(call print_separator,$(GREEN))
print_line_blue = $(call print_separator,$(BLUE))
print_line_red = $(call print_separator,$(RED))


###############################################################################
##@ Help
###############################################################################

.PHONY: help

help:  ## Display this help message
	@printf "\n"
	$(print_line_blue)
	printf "$(BLUE)Jupyter Notebook and Kernel Validation $(CYAN)Makefile$(NC)\n"
	printf "    This makefile has been created to simplify the execution of\n"
	printf "    Jupyter notebook and kernel validation routines.\n"
	$(print_line_blue)
	printf "\n"
	$(print_line_blue)
	printf "$(BLUE)Usage\n"
	printf "    $(CYAN)make $(NC)<target>                "
	printf "Execute from the list of targets below\n"
	awk 'BEGIN {FS = ":.*##";} /^[a-zA-Z_-].*##/ \
	{ printf "    $(CYAN)%-27s$(NC) %s\n", $$1, $$2} /^##@/ \
	{ printf "\n$(BLUE)%s$(NC)\n", substr($$0, 5) } ' $(MAKEFILE_LIST)
	$(print_line_blue)
	printf "\n"


###############################################################################
##@ MAXIV Validations - Run apptainer locally, use apptainer kernel images
###############################################################################

# Executables and directories to mount when running jnbv image
# Need to mount: apptainer, kernels, kernel images
APPTAINER_BINDPATH = "/usr/bin/singularity,/usr/bin/apptainer,/etc/apptainer,/usr/libexec/apptainer,/var/lib/apptainer,/sw/jupyterhub/jupyter-kernels-apptainer/kernel-spec-files/share/jupyter:/usr/local/share/jupyter,/sw/jupyterhub/jupyter-kernels-apptainer/sif-img-files,/mxn/groups/kits/scisw/jupyterhub"
XDG_RUNTIME_DIR = /tmp/$$(id -u)
OUTPUTDIR = "/mxn/groups/kits/scisw/jupyterhub/jupyter-notebook-validation-test-results"

list-maxiv-kernels: ## List the MAXIV kernel apptainer images available
	export APPTAINER_BINDPATH=$(APPTAINER_BINDPATH)
	export XDG_RUNTIME_DIR=$(XDG_RUNTIME_DIR)
	apptainer run /sw/jupyterhub/jnbv/apptainer/sif-img-files/jnbv.sif \
		jupyter kernelspec list

balder: validate-kernel-balder ## Balder kernel
bloch: validate-kernel-bloch ## Bloch kernel
danmax: validate-kernel-danmax ## DanMax kernel
danmax-dev: validate-kernel-danmax-dev ## DanMax-dev kernel
formax: validate-kernel-formax ## ForMax kernel
hdf5: validate-kernel-hdf5 ## HDF5 kernel
hdf5-old-azint: validate-kernel-old-azint ## HDF5 Old Azint kernel
maxpeem: validate-kernel-maxpeem ## MaxPeem kernel
tomography-imquant: validate-kernel-tomography-imquant ## Tomography-Imquant kernel
tomography-rectv: validate-kernel-tomography- rectv ## Tomography-Rectv kernel
tomography-spam: validate-kernel-tomography-spam ## Tomography-Spam kernel
tomography-tomorec: validate-kernel-tomography-tomorec ## Tomography-Tomorec kernel
tomography-xphase: validate-kernel-tomography-xphase ## Tomography-Xphase kernel

validate-kernel-%:
	export APPTAINER_BINDPATH=$(APPTAINER_BINDPATH)
	export XDG_RUNTIME_DIR=$(XDG_RUNTIME_DIR)
	# NOTEBOOKS=notebooks/maxiv/$*/01*.ipynb
	NOTEBOOKS=notebooks/maxiv/$*/*.ipynb
	case "$*" in
		hdf5)
			NOTEBOOKS=notebooks/maxiv/hdf5/0[2-3]*.ipynb ;;
		hdf5-old-azint)
			NOTEBOOKS=notebooks/maxiv/hdf5/01*.ipynb ;;
	esac
	time \
		apptainer run --nv \
			/sw/jupyterhub/jnbv/apptainer/sif-img-files/jnbv.sif \
			jnbv \
			--kernel maxiv-jup-kernel-$* \
			--validate $$NOTEBOOKS \
			--save_dir $(OUTPUTDIR)


###############################################################################
##@ MAXIV Validations - HPC slurm jobs that use apptainer kernel images
###############################################################################

slurm-balder: slurm-validate-kernel-balder ## Balder kernel
slurm-bloch: slurm-validate-kernel-bloch ## Bloch kernel
slurm-danmax: slurm-validate-kernel-danmax ## DanMax kernel
slurm-danmax-dev: slurm-validate-kernel-danmax-dev ## DanMax-dev kernel
slurm-formax: slurm-validate-kernel-formax ## Formax kernel
slurm-hdf5: slurm-validate-kernel-hdf5 ## HDF5 kernel
slurm-hdf5-old-azint: slurm-validate-kernel-hdf5-old-azint ## HDF5 Old Azint kernel
slurm-maxpeem: slurm-validate-kernel-maxpeem ## MaxPeem kernel
slurm-tomography-imquant: slurm-validate-kernel-tomography-imquant ## Tomography-Imquant kernel
slurm-tomography-rectv: slurm-validate-kernel-tomography-rectv ## Tomography-Rectv kernel
slurm-tomography-spam: slurm-validate-kernel-tomography-spam ## Tomography-Spam kernel
slurm-tomography-tomorec: slurm-validate-kernel-tomography-tomorec ## Tomography-Tomorec kernel
slurm-tomography-xphase: slurm-validate-kernel-tomography-xphase ## Tomography-Xphase kernel

slurm-validate-kernel-%:
	@printf "\n"
	export APPTAINER_BINDPATH=$(APPTAINER_BINDPATH)
	NOTEBOOKS=notebooks/maxiv/$*/*.ipynb
	PARTITION=all,v100
	case "$*" in
		danmax|formax|hdf5|hdf5-old-azint|tomography-rectv|tomography-tomorec)
			PARTITION=v100 ;;&
		danmax)
			NOTEBOOKS=notebooks/maxiv/$*/0*.ipynb ;;
		hdf5-old-azint)
			NOTEBOOKS=notebooks/maxiv/hdf5/*.ipynb ;;
		tomography-rectv)
			NOTEBOOKS=notebooks/maxiv/$*/0[0-1,3]*.ipynb ;;
		tomography-tomorec)
			NOTEBOOKS=notebooks/maxiv/$*/0[0-4]*.ipynb ;;
	esac
	time \
		bash slurm-scripts/execute-slurm-job.sh \
			maxiv-jup-kernel-$* \
			"$$NOTEBOOKS" \
			$$PARTITION \
			$(OUTPUTDIR)
	printf "\n"
