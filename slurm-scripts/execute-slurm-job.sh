#!/bin/bash

## Colors 
LIGHTPURPLE="\033[1;35m"
GREEN="\033[32m"
BLUE="\033[1;34m"
RED="\033[31m"
NC="\033[0m"
BOLD="\033[1m"
NORMAL=$(tput sgr0)

# Get input
export KERNEL=$1
export NOTEBOOKS=$2
export PARTITION=$3
export OUTPUTDIR=$4

############################################################
# TODO: Add checks for above input, and APPTAINER_BINDPATH #
############################################################

export XDG_RUNTIME_DIR=/tmp/$USER

printf " ${BLUE}** VALIDATION VIA SLURM BATCH JOB${NC}\n"
printf " ${BLUE}**${NC}            Kernel:   $KERNEL\n"
printf " ${BLUE}**${NC}         Notebooks:   $NOTEBOOKS\n"
printf " ${BLUE}**${NC}         Partition:   $PARTITION\n"
printf " ${BLUE}**${NC}         Outputdir:   $OUTPUTDIR\n"
printf " ${BLUE}**${NC}APPTAINER_BINDPATH:   $APPTAINER_BINDPATH\n"

# Need set options inorder to tkae entire node, as some notebooks need this
options="-N 1 -c 2 --exclusive --mem=0"

printf " ${BLUE}**${NC}           options:   $options\n"

# Submit batch job, wait until finished, then grab the job id
date_stamp=$(date +%Y-%m-%d_%H-%M-%S)
output_log=$PWD/slurm-logs/jup_val-$KERNEL-$date_stamp.log
printf " ${BLUE}**${NC}         submitted:   $date_stamp\n"
printf " ${BLUE}**${NC}          log file:   $output_log\n"
JOBID=$(
    sbatch -W --parsable \
        --job-name=jup-val \
        --chdir=$PWD \
        --output=$output_log \
        --partition=$PARTITION \
        $options \
        slurm-scripts/slurm-job.slurm
)
printf " ${BLUE}**${NC}             JOBID:   $JOBID\n"
printf " ${BLUE}**${NC}          finished:   $(date +%Y-%m-%d_%H-%M-%S)\n"
printf " ${BLUE}** VALIDATION BATCH JOB DONE${NC}\n"

# Print the slurm log to the terminal
if [ ! -f "$output_log" ]; then
    printf "${RED}No slurm log file for validation tests${NC}\n"
    exit 1
fi
cat $output_log 

# Look for assertion errors and other errors in the slurm log
ASSERTIONERROR=$(grep -e "AssertionError" -e "Error 1" $output_log)
if [ -n "$ASSERTIONERROR" ]; then
    printf "${RED}Error in slurm batch validation tests${NC}\n"
    exit 1
fi

exit 0
