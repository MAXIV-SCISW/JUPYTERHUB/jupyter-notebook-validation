import os
import sys

from typing import Tuple, Dict
from dataclasses import dataclass
from datetime import datetime, timedelta
import time
import json
import argparse
import re

import kubernetes.client as kc
import kubernetes.config as kconf
import kubernetes.watch as kwatch

# Make it purrty
LIGHTPURPLE = "\033[1;35m"
GREEN = "\033[0;32m"
BLUE = "\033[34m"
CYAN = "\033[1;36m"
RED = "\033[31m"
NC = "\033[0m"

# Kubernetes Persistent Volume Claim names.
VALIDATIONS_DATA = 'kits-scisw-jupyterhub-data-for-validations'
VALIDATIONS_NOTEBOOKS = 'kits-scisw-jupyterhub-jupyter-notebook-validation'
VALIDATION_RESULTS = 'kits-scisw-jupyterhub-jupyter-notebook-validation-test-results'
MATLAB_LIBRARY = 'kits-scisw-jupyterhub-notebook-validation-matlab-r2020b'

VALIDATION_DIR_IMAGE_PATH = '/home/jovyan/jupyter-notebook-validation/'
DATA_DIR_IMAGE_PATH = '/mxn/groups/kits/scisw/jupyterhub/data-for-validations/'
MATLAB_DIR_IMAGE_PATH = '/usr/local/MATLAB/R2020b/'
VALIDATION_TEST_RESULTS_PATH = '/mxn/groups/kits/scisw/jupyterhub/jupyter-notebook-validation-test-results/'

JOB_TIMEOUT_SEC = 1800
JOB_TTL_SECONDS_AFTER_FINISHED = 86400

JOB_RUN_AS_USER = 1416
JOB_RUN_AS_GROUP = 1300


@dataclass
class JobConfig:
    ci_job_id: str
    ci_project_name: str
    ci_project_path: str
    ci_commit_branch: str
    ci_commit_tag: str
    ci_commit_author: str
    ci_commit_sha: str
    ci_job_name: str
    ci_commit_ref_name: str
    ci_commit_message: str

    script: str
    image: str
    helper_image: str
    job_name: str
    job_namespace: str

    job_run_timeout_sec: int

    validation_notebooks_url: str
    jupyter_docker_stacks_git_url: str

    cpu_requests: str
    cpu_limits: str
    memory_requests: str
    memory_limits: str


def create_job(job_config: JobConfig) -> kc.V1Job:
    spaces_re = re.compile(r'\s+')

    object_meta = kc.V1ObjectMeta(
        annotations={
            'maxiv.lu.se/gitlab-commit-ref-name':
                job_config.ci_commit_ref_name,
            'maxiv.lu.se/gitlab-commit-message': job_config.ci_commit_message,
            'maxiv.lu.se/gitlab-commit-branch': job_config.ci_commit_branch,
            'maxiv.lu.se/gitlab-commit-tag': job_config.ci_commit_tag,
            'maxiv.lu.se/gitlab-commit-author': job_config.ci_commit_author,
            'maxiv.lu.se/gitlab-project-path': job_config.ci_project_path
        },
        labels={
            'maxiv.lu.se/gitlab-job-id':
                spaces_re.sub('-', job_config.ci_job_id.lower()),
            'maxiv.lu.se/gitlab-project-name':
                spaces_re.sub('-', job_config.ci_project_name.lower()),
            'maxiv.lu.se/gitlab-commit-sha': job_config.ci_commit_sha.lower(),
            'maxiv.lu.se/gitlab-job-name':
                spaces_re.sub('-', job_config.ci_job_name.lower()),
        },
    )

    env_vars = [
        {
            'name': 'DATA_DIR_IMAGE_PATH',
            'value': DATA_DIR_IMAGE_PATH,
        },
        {
            'name': 'MATLAB_DIR_IMAGE_PATH',
            'value': MATLAB_DIR_IMAGE_PATH,
        },
        {
            'name': 'VALIDATION_DIR_IMAGE_PATH',
            'value': VALIDATION_DIR_IMAGE_PATH,
        },
        {
            'name': 'VALIDATION_TEST_RESULTS_PATH',
            'value': VALIDATION_TEST_RESULTS_PATH,
        },
        {
            'name': 'JUPYTER_NOTEBOOK_VALIDATION_GIT_URL',
            'value': job_config.validation_notebooks_url,
        },
        {
            'name': 'JOB_SCRIPT',
            'value': job_config.script,
        },
        {
            'name': 'JUPYTER_DOCKER_STACKS_CI_COMMIT_REF_NAME',
            'value': job_config.ci_commit_ref_name,
        },
        {
            'name': 'JUPYTER_DOCKER_STACKS_GIT_URL',
            'value': job_config.jupyter_docker_stacks_git_url,
        },
        {
            'name': 'NVIDIA_VISIBLE_DEVICES',
            'value': 'all'
        }
    ]

    volumes = [
        kc.V1Volume(
            empty_dir=kc.V1EmptyDirVolumeSource(),
            name='run-script',
        ),
        kc.V1Volume(
            empty_dir=kc.V1EmptyDirVolumeSource(),
            name='var-www-jupyterhub-jupyter-notebook-validation'
        ),
        kc.V1Volume(
            empty_dir=kc.V1EmptyDirVolumeSource(),
            name='var-www-jupyterhub-jupyter-docker-stacks'
        ),
        # Do not mount /mnt/groups... validation Notebooks
        # Clone from Git instead to emptyDir
        #   "var-www-jupyterhub-jupyter-notebook-validation"
        # ToDo: delete commented code if not used anymore.
        #
        # kc.V1Volume(
        #     persistent_volume_claim=kc.V1PersistentVolumeClaimVolumeSource(
        #         claim_name=VALIDATIONS_NOTEBOOKS,
        #         read_only=True,
        #     ),
        #     name=VALIDATIONS_NOTEBOOKS,
        # ),
        kc.V1Volume(
            persistent_volume_claim=kc.V1PersistentVolumeClaimVolumeSource(
                claim_name=MATLAB_LIBRARY,
                read_only=True,
            ),
            name=MATLAB_LIBRARY,
        ),
        kc.V1Volume(
            persistent_volume_claim=kc.V1PersistentVolumeClaimVolumeSource(
                claim_name=VALIDATIONS_DATA,
                read_only=False,
            ),
            name=VALIDATIONS_DATA,
        ),
        kc.V1Volume(
            persistent_volume_claim=kc.V1PersistentVolumeClaimVolumeSource(
                claim_name=VALIDATION_RESULTS,
                read_only=False,
            ),
            name=VALIDATION_RESULTS,
        ),
        kc.V1Volume(
            empty_dir=kc.V1EmptyDirVolumeSource(),
            name='home-jovyan',
        )
    ]

    volume_mounts = [
        kc.V1VolumeMount(
            mount_path='/run-script',
            name='run-script',
        ),
        kc.V1VolumeMount(
            mount_path='/var/www/jupyterhub/jupyter-notebook-validation/',
            name='var-www-jupyterhub-jupyter-notebook-validation',
        ),
        kc.V1VolumeMount(
            mount_path=VALIDATION_DIR_IMAGE_PATH,
            name='var-www-jupyterhub-jupyter-notebook-validation',
        ),
        kc.V1VolumeMount(
            mount_path='/var/www/jupyterhub/jupyter-docker-stacks/',
            name='var-www-jupyterhub-jupyter-docker-stacks',
        ),
        # Do not mount /mnt/groups... validation Notebooks
        # Clone from Git instead to emptyDir
        #   "var-www-jupyterhub-jupyter-notebook-validation"
        # ToDo: delete commented code if not used anymore.
        #
        # kc.V1VolumeMount(
        #     mount_path=VALIDATION_DIR_IMAGE_PATH,
        #     name=VALIDATIONS_NOTEBOOKS,
        # ),
        kc.V1VolumeMount(
            mount_path=MATLAB_DIR_IMAGE_PATH,
            name=MATLAB_LIBRARY,
        ),
        kc.V1VolumeMount(
            mount_path=DATA_DIR_IMAGE_PATH,
            name=VALIDATIONS_DATA,
        ),
        kc.V1VolumeMount(
            mount_path=VALIDATION_TEST_RESULTS_PATH,
            name=VALIDATION_RESULTS,
        ),
        kc.V1VolumeMount(
            mount_path='/home/jovyan/',
            name='home-jovyan',
        )
    ]

    image_pull_policy = 'Always'

    job = kc.V1Job(
        metadata=kc.V1ObjectMeta(
            annotations=object_meta.annotations,
            labels=object_meta.labels,
            name=job_config.job_name,
        ),
        spec=kc.V1JobSpec(
            active_deadline_seconds=job_config.job_run_timeout_sec,
            backoff_limit=0,  # Do not retry to run the job - fail on first failure
            ttl_seconds_after_finished=JOB_TTL_SECONDS_AFTER_FINISHED,
            template=kc.V1Pod(
                metadata=kc.V1ObjectMeta(
                    annotations=object_meta.annotations,
                    labels=object_meta.labels,
                ),
                spec=kc.V1PodSpec(
                    volumes=volumes,
                    restart_policy='Never',
                    security_context=kc.V1PodSecurityContext(
                        fs_group=JOB_RUN_AS_GROUP,
                        run_as_user=JOB_RUN_AS_USER,
                        run_as_group=JOB_RUN_AS_GROUP,
                    ),
                    init_containers=[
                        kc.V1Container(
                            name='git-clone-validation-notebooks',
                            image=job_config.helper_image,
                            image_pull_policy=image_pull_policy,
                            env=env_vars,
                            resources=kc.V1ResourceRequirements(
                                limits={
                                    'cpu': '200m',
                                    'memory': '128Mi'
                                },
                                requests={
                                    'cpu': '100m',
                                    'memory': '64Mi'
                                },
                            ),
                            command=[
                                'bash',
                                '-c',
                                '''
                                    echo "Pull newest code from git repo $JUPYTER_NOTEBOOK_VALIDATION_GIT_URL" &&
                                    cd /var/www/jupyterhub/jupyter-notebook-validation/ &&
                                    git clone --depth 1 "$JUPYTER_NOTEBOOK_VALIDATION_GIT_URL" .
                                '''
                            ],
                            volume_mounts=volume_mounts,
                        ),
                        kc.V1Container(
                            name='pull-code-jupyter-docker-stacks',
                            image=job_config.helper_image,
                            image_pull_policy=image_pull_policy,
                            env=env_vars,
                            command=[
                                'bash',
                                '-c',
                                '''
                                date +%Y-%m-%d_%H-%M-%S &&
                                hostname &&
                                id &&

                                echo "Pull newest code from gitlab repo $JUPYTER_DOCKER_STACKS_GIT_URL to /var/www/jupyterhub/jupyter-docker-stacks/" &&
                                echo "using shallow clone for 50 latest commits" &&
                                cd /var/www/jupyterhub/jupyter-docker-stacks/ &&
                                pwd &&
                                git clone -b $JUPYTER_DOCKER_STACKS_CI_COMMIT_REF_NAME --depth 50 $JUPYTER_DOCKER_STACKS_GIT_URL .
                                git reset &&
                                git checkout . &&
                                git clean -fdx &&
                                git fetch origin &&
                                git checkout $JUPYTER_DOCKER_STACKS_CI_COMMIT_REF_NAME &&
                                git log -1
                                ''',
                            ],
                            resources=kc.V1ResourceRequirements(
                                limits={
                                    'cpu': '200m',
                                    'memory': '128Mi',
                                },
                                requests={
                                    'cpu': '100m',
                                    'memory': '64Mi',
                                },
                            ),
                            volume_mounts=volume_mounts,
                        ),
                        kc.V1Container(
                            name='init-script',
                            image=job_config.helper_image,
                            image_pull_policy=image_pull_policy,
                            env=env_vars,
                            command=[
                                'bash',
                                '-c',
                                'echo -e "${JOB_SCRIPT}" > /run-script/script.sh',
                            ],
                            resources=kc.V1ResourceRequirements(
                                limits={
                                    'cpu': '100m',
                                    'memory': '64Mi'
                                },
                                requests={
                                    'cpu': '100m',
                                    'memory': '64Mi'
                                },
                            ),
                            volume_mounts=volume_mounts,
                        ),
                    ],
                    containers=[
                        kc.V1Container(
                            name='run-job',
                            image=job_config.image,
                            image_pull_policy=image_pull_policy,
                            command=[
                                'bash',
                                '/run-script/script.sh',
                            ],
                            env=env_vars,
                            resources=kc.V1ResourceRequirements(
                                limits={
                                    'cpu': job_config.cpu_limits,
                                    'memory': job_config.memory_limits,
                                },
                                requests={
                                    'cpu': job_config.cpu_requests,
                                    'memory': job_config.memory_requests,
                                },
                            ),
                            volume_mounts=volume_mounts,
                        ),
                    ],
                ),
            )
        ),
    )

    return job


def getenv_str(key: str) -> str:
    return str(os.getenv(key, ''))


def load_job_config(job_timeout: timedelta) -> JobConfig:
    script = getenv_str('K8S_REMOTE_CMD')
    if len(script) == 0:
        raise ValueError('Environment variable K8S_REMOTE_CMD must be set')

    # image = getenv_str('NB_IMAGE_SETUP_reg_proj_name_tag')
    # if len(image) == 0:
    #     raise ValueError(
    #         'Environment variable NB_IMAGE_SETUP_reg_proj_name_tag
    #           must be set')

    image = getenv_str('REG_PROJ_IMG_TAG')
    if len(image) == 0:
        raise ValueError(
            'Environment variable REG_PROJ_IMG_TAG must be set')

    ci_project_name = getenv_str('CI_PROJECT_NAME')
    if len(ci_project_name) == 0:
        raise ValueError(
            'Environment variable CI_PROJECT_NAME must be set by GitLab CI')

    ci_job_id = getenv_str('CI_JOB_ID')
    if len(ci_job_id) == 0:
        raise ValueError(
            'Environment variable CI_JOB_ID must be set by GitLab CI')

    ci_commit_sha = getenv_str('CI_COMMIT_SHA')
    if len(ci_commit_sha) == 0:
        raise ValueError(
            'Environment variable CI_COMMIT_SHA must be set by GitLab CI')

    jupyter_docker_stacks_url = getenv_str('JUPYTER_DOCKER_STACKS_GIT_URL')
    if len(jupyter_docker_stacks_url) == 0:
        raise ValueError(
            'Environment variable JUPYTER_DOCKER_STACKS_GIT_URL '
            'must be set by GitLab CI')

    jupyter_notebook_validation_url = getenv_str(
        'JUPYTER_VALIDATIONS_NOTEBOOKS_GIT_URL')
    if len(jupyter_docker_stacks_url) == 0:
        raise ValueError(
            'Environment variable JUPYTER_VALIDATIONS_NOTEBOOKS_GIT_URL '
            'must be set by GitLab CI')

    jupyter_helper_container_image = getenv_str(
        'JUPYTER_HELPER_CONTAINER_IMAGE')
    if len(jupyter_docker_stacks_url) == 0:
        raise ValueError(
            'Environment variable JUPYTER_HELPER_CONTAINER_IMAGE must '
            'be set by GitLab CI')

    job_name = getenv_str('JOB_NAME')
    if len(job_name) == 0:
        # Default generated name.
        tmp = (getenv_str('CI_PROJECT_NAME') +
               '-' + getenv_str('CI_JOB_ID') +
               '-' + getenv_str('CI_COMMIT_SHA'))
        job_name = tmp[:50]

    job_namespace = getenv_str('JOB_NAMESPACE')
    if len(job_namespace) == 0:
        raise ValueError(
            'Environment variable JOB_NAMESPACE must be set by GitLab CI')

    cpu_requests = getenv_str('VALIDATION_CPU_REQUESTS')
    if len(cpu_requests) == 0:
        raise ValueError(
            'Environment variable VALIDATION_CPU_REQUESTS must be set by '
            'GitLab CI')

    cpu_limits = getenv_str('VALIDATION_CPU_LIMITS')
    if len(cpu_requests) == 0:
        raise ValueError(
            'Environment variable VALIDATION_CPU_LIMITS must be set by '
            'GitLab CI')

    memory_requests = getenv_str('VALIDATION_MEMORY_REQUESTS')
    if len(cpu_requests) == 0:
        raise ValueError(
            'Environment variable VALIDATION_MEMORY_REQUESTS must be set by '
            'GitLab CI')

    memory_limits = getenv_str('VALIDATION_MEMORY_LIMITS')
    if len(cpu_requests) == 0:
        raise ValueError(
            'Environment variable VALIDATION_MEMORY_LIMITS must be set by '
            'GitLab CI')

    job_config = JobConfig(
        ci_commit_author=getenv_str('CI_COMMIT_AUTHOR'),
        ci_commit_branch=getenv_str('CI_COMMIT_BRANCH'),
        ci_commit_message=getenv_str('CI_COMMIT_MESSAGE'),
        ci_commit_ref_name=getenv_str('CI_COMMIT_REF_NAME'),
        ci_commit_sha=ci_commit_sha,
        ci_commit_tag=getenv_str('CI_COMMIT_TAG'),
        ci_job_id=ci_job_id,
        ci_job_name=job_name,
        ci_project_name=ci_project_name,
        ci_project_path=getenv_str('CI_PROJECT_PATH'),
        image=image,
        helper_image=jupyter_helper_container_image,
        script=script,
        job_name=job_name,
        job_namespace=job_namespace,
        job_run_timeout_sec=int(job_timeout.total_seconds()),
        validation_notebooks_url=jupyter_notebook_validation_url,
        jupyter_docker_stacks_git_url=jupyter_docker_stacks_url,
        memory_limits=memory_limits,
        memory_requests=memory_requests,
        cpu_limits=cpu_limits,
        cpu_requests=cpu_requests,
    )
    return job_config


def prepare_label_selector(labels: Dict[str, str]) -> str:
    selector = []

    for k, v in labels.items():
        selector.append(f'{k}={v}')

    return ','.join(selector)


def is_job_succeeded(job: kc.V1Job) -> Tuple[bool, Dict]:
    w = kwatch.Watch()

    for job_event in w.stream(
            job_kclient.list_namespaced_job,
            job_config.job_namespace,
            label_selector=prepare_label_selector(k8s_job.metadata.labels)):
        if job_event['type'] != 'MODIFIED':
            continue

        event_object = job_event['object']

        if event_object.status.failed is not None:
            w.stop()

            return False, event_object

        if event_object.status.succeeded is not None:
            w.stop()

            return True, event_object


def print_lines(logs_color: str, logs: str):
    for line in logs.splitlines():
        print(logs_color, '**', line, NC)


def print_job_logs(pod_selector: str, logs_color: str):
    pod_kclient = kc.CoreV1Api()

    # Get Pods (must be 1 Pod only as the Job is configured with backoff_limit
    # = 0).
    job_pods = pod_kclient.list_namespaced_pod(
        k8s_job.metadata.namespace, label_selector=pod_selector).items

    if len(job_pods) > 0:
        print(logs_color, '*'.center(80, '*'), NC)
        print(logs_color, '**', 'Job Logs:', NC)
        for pod in job_pods:
            print(logs_color, '**',
                  f'Pod {pod.metadata.name} in {pod.metadata.namespace}:', NC)
            print(logs_color, '**', NC)

            print(logs_color, '**', 'Init containers\' logs:', NC)
            for init_container in pod.status.init_container_statuses:
                # Skip not started containers.
                if init_container.state.waiting:
                    continue

                logs: str = pod_kclient.read_namespaced_pod_log(
                    pod.metadata.name, pod.metadata.namespace,
                    container=init_container.name)

                print(logs_color, '**',
                      f'Init container {init_container.name} logs:', NC)
                print_lines(logs_color, logs)

            print(logs_color, '**', NC)

            print(logs_color, '**', 'Task run containers\' logs:', NC)
            for container in pod.status.container_statuses:
                # Skip not started containers.
                if container.state.waiting:
                    continue

                print(logs_color, '**',
                      f'Container {container.name} logs', NC)

                logs: str = pod_kclient.read_namespaced_pod_log(
                    pod.metadata.name, pod.metadata.namespace,
                    container=container.name)
                print_lines(logs_color, logs)

        print(logs_color, '*'.center(80, '*'), NC)


if __name__ == '__main__':
    now = datetime.now()
    max_run_time = timedelta(seconds=JOB_TIMEOUT_SEC)
    deadline = now+max_run_time

    job_config = load_job_config(max_run_time)
    job = create_job(job_config)

    # Load Kubernetes client config
    kconf.load_kube_config()

    print('')
    print(CYAN, '*'.center(80, '*'), NC)
    print(CYAN, '**', f'Starting Kubernetes Job '
          f'{job_config.job_name} '
          f'in namespace {job_config.job_namespace}', NC)
    print(CYAN, '**', f'Start time: {now.isoformat()}', NC)
    print(CYAN, '**', f'Deadline at: {deadline.isoformat()}', NC)
    print(CYAN, '**', f'Using Image: {job_config.image}', NC)

    # Create a Job
    job_kclient = kc.BatchV1Api()
    k8s_job = job_kclient.create_namespaced_job(job_config.job_namespace, job)

    print(CYAN, '**')
    print(CYAN, '**',
          f'Started Kubernetes Job {job_config.job_name} '
          f'in namespace {job_config.job_namespace}', NC)
    print(CYAN, '*'.center(80, '*'), NC)

    # Wait until Job finish.
    succeeded, event = is_job_succeeded(k8s_job)

    if succeeded:
        logs_color = GREEN

        print(GREEN, '*'.center(80, '*'), NC)
        print(GREEN, '**', f'Kubernetes Job '
              f'{job_config.job_name} '
              f'in namespace {job_config.job_namespace}', NC)
        print(GREEN, '**', f'completed at: {event.status.completion_time}', NC)
        print(GREEN, '*'.center(80, '*'), NC)
    else:
        logs_color = RED

        print(RED, '*'.center(80, '*'), NC)
        print(RED, '**', f'Kubernetes Job '
              f'{job_config.job_name} '
              f'in namespace {job_config.job_namespace}', NC)
        print(RED, '**', f'Failed at: {datetime.now()}', NC)
        print(RED, '**', 'Error events:',
              json.dumps(event.status.conditions, default=str), NC)
        print(RED, '*'.center(80, '*'), NC)

    # Prepare selector to get Pods for the Job.
    pod_selector = prepare_label_selector(
        k8s_job.spec.template.metadata.labels)

    # Print logs.
    print_job_logs(pod_selector, logs_color)

    if not succeeded:
        sys.exit(1)
