# JUPYTER NOTEBOOK VALIDATION

1. [Project Overview](#project-overview)
2. [General Usage](#general-usage)
3. [MAX-IV Manual Usage](#max-iv-manual-usage)
4. [MAX-IV CI Usage](#max-iv-ci-usage)


## PROJECT OVERVIEW

This repository is meant make use of the python module jnbv:
- jnbv repository: [![gitlab jnbv](https://badgen.net/badge/icon/gitlab?icon=gitlab&label=jnbv)](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv)
- jnbv PyPi package: [![PyPI jnbv](https://img.shields.io/pypi/v/jnbv)](https://pypi.org/project/jnbv/)

The jnbv (Jupyter Notebook Validation) module is used to:
1. Execute notebooks in the terminal
2. Check execution output for errors
3. Compare output of an execution to known output
4. Choose different jupyter kernels to use
5. Log results of notebook executions and tests

The contents of this repository:
1. Around 50 ipynb notebooks used for testing particular Jupyter kernels in use
   at MAX IV Laboratory and other institutions.
2. A Makefile which simplifies the execution of the validation tests via docker
   images and slurm batch jobs.

As of 2025-01-02 this repository is NOT setup to run as part of the gitlab CI
at MAX IV Laboratory for use with several installations of JupyteHub.<br>
See the section towards the end of this readme [MAX-IV CI Use](#max-iv-ci-use)
for more information.<br>
These setups could ofcourse could be altered to run at other facilities.


## INSTALL
There are two things that need to be installed:
1. An apptainer image containing the jnbv module needs to be available.
2. This jupyter-notebook-validation repository needs to be cloned somewhere
   that is accessible.


### JNBV MODULE INSTALLATION
The simplest and least intrusive method for using jnbv is to have an apptainer
image available in which jnbv has been installed. See the
[jnbv apptainer documentation](https://gitlab.maxiv.lu.se/scisw/jupyterhub/jnbv#apptainer)
inorder to create this image.

Once the image is created, the apptiner environmental variable
*APPTAINER_BINDPATH* can be set to mount existing kernels into the jnbv image.

Once setup, jnbv can be used to:
- execute notebooks using specified kernels
- read the resulting output from an execution
- test execution outputs for errors
- compare the output between different executions of the same notebook

For example, the execution of a notebook with the *maxiv-jup-kernel-hdf5*
kernel:
```bash
jnbv the-meaning-of-life.ipynb --kernel_name python3 --execute
apptainer run --nv /sw/jupyterhub/jnbv/apptainer/sif-img-files/jnbv.sif \
    jnbv \
    --kernel maxiv-jup-kernel-hdf5
    --validate notebooks/maxiv/hdf5/*.ipynb \
    --save_dir test-output-dir
```

For more examples, see the
[jnbv README](https://gitlab.maxiv.lu.se/scisw/jupyterhub/jnbv)


### JUPYTER-NOTEBOOK-VALIDATION REPOSITORY INSTALLATION
As these validation tools may need to be accessed by many different user
accounts, the correct setting of permissions is important.  Not doing so
can cause headaches later on.

Go to a directory where all necessary users can access the files, then set the
umask and get the code:
```bash
umask 0002
git clone https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jupyter-notebook-validation.git
cd jupyter-notebook-validation/
```


## USAGE

### GENERAL USAGE

Once configured, the usage of the validation tools is meant to be
straightforward, though it can be confusing at first with the setting of the
environmental variable *APPTAINER_BINDPATH*.

The makefile in this repository contains some examples which can be run with
makefile targets, assumed to be run from one of the HPC front-ends.

For example, to run all the Bloch kernel validations directly on the machine
where this repository is installed:
```bash
make bloch
```

This target sets the *APPTAINER_BINDPATH* variable:
```bash
export APPTAINER_BINDPATH="/usr/bin/singularity,/usr/bin/apptainer,/etc/apptainer,/usr/libexec/apptainer,/var/lib/apptainer,/sw/jupyterhub/jupyter-kernels-apptainer/kernel-spec-files/share/jupyter:/usr/local/share/jupyter,/sw/jupyterhub/jupyter-kernels-apptainer/sif-img-files,/mxn/groups/kits/scisw/jupyterhub"
```


And the same validation tests, but this time run on an HPC cluster by
submitting a job via slurm:
```bash
make slurm-bloch
```

The above commands will use the given kernel to execute each ipynb notebook
that exists for that kernel:
```bash
notebooks/maxiv/bloch/
├── 00-the-meaning-of-life.ipynb
├── 01-load-modules-bloch.ipynb
├── 02-hdf5-h5py-eiger-data.ipynb
├── 03-hdf5-h5py-read-plot.ipynb
├── 04-example_plot_simple_ipywidget.ipynb
└── 05-example_plots_matplotlib_ipywidget.ipynb
```

The results of the notebook execution will appear in the terminal, and in the
end a summary of the results will be displayed:

![screenshot_validation_results_hdf5](screenshots/screenshot_validation_results_hdf5.png)

The results are also saved into log files and ipynb notebooks, for example:
```bash
test-results/maxiv-jhub-hpc-kernel-hdf5/
└── 2021-03-08_13-24-51/
    ├── 00-the-meaning-of-life.ipynb
    ├── 00-the-meaning-of-life.log
    ├── 01-load-modules-hdf5.ipynb
    ├── 01-load-modules-hdf5.log
    ├── 02-hdf5-h5py-eiger-data.ipynb
    ├── 02-hdf5-h5py-eiger-data.log
    ├── 03-hdf5-h5py-read-plot.ipynb
    ├── 03-hdf5-h5py-read-plot.log
    ├── 04-bh_01_simple-using-cupy.ipynb
    ├── 04-bh_01_simple-using-cupy.log
    ├── 05-example_plots_matplotlib_ipywidget.ipynb
    ├── 05-example_plots_matplotlib_ipywidget.log
    ├── 06-numba-basics.ipynb
    └── 06-numba-basics.log
```

The output ipynb notebooks allow visual inspection of the notebook execution.
When an errors occur, the location of the errors is easily seen:

![error output notebook](screenshots/error_display_notebook.png)

For more running options, see:
```bash
make
```

![makefile screenshot](screenshots/makefile_screenshot.png)


### MAX-IV MANUAL USAGE

The easiest way to use the validation tests and notewbooks in this repository
is to use the HPC.

For example, say I want to test the standard HDF5 kernel:
```bash
ssh w-kirk19-clu0-fe-2.maxiv.lu.se
cd /mxn/groups/kits/scisw/jupyterhub/jupyter-notebook-validation/
make slurm-hdf5
```

After 3 minutes or so, the results of the validation will be displayed in the
terminal, and the resulting notebooks and logs will be placed here:
```bash
/mxn/groups/kits/scisw/jupyterhub/jupyter-notebook-validation-test-results/
└── maxiv-jup-kernel-hdf5/
```


### MAX-IV CI USAGE

*As of 2025-01-02 the CI is not working!!*
The CI needs to be updated for new runners and Juptyer kernels.

#### VALIDATION CHECKS
The [CI configuration](.gitlab-ci.yml) in this repository is setup to run
validation tests with all kernels in both the docker and HPC installations.
The main use of this is to run periodic checks to make sure everything is
running as it should (network, storage, kernels, docker, clusters, etc.) and
if there is a problem somewhere, then most likely most of the validation tests
will fail.

##### RUN ALL VALIDATION CHECKS
A [pipeline schedule](https://gitlab.maxiv.lu.se/scisw/jupyterhub/jupyter-notebook-validation/-/pipeline_schedules)
has been setup to run all the validation tests every day at 22:00.  This can
be found in the left hand gitlab menu under "CI/CD > Schedules".

The scheduled pipeline can also be manually triggered at any time by clicking
the "play" button on the right hand side of the schedule page, as seen in the
screenshot below:

![pipeline-schedules](screenshots/pipeline-schedules.png)

One may view the outcome of previously run pipelines and monitor the progress
of the currently running pipelines from the
[CI/CD pipelines page.](https://gitlab.maxiv.lu.se/scisw/jupyterhub/jupyter-notebook-validation/-/pipelines)

Pipelines that have been run at a scheduled time are marked with a "Scheduled"
icon, with the most recent running marked as "latest", as seen in the following
screenshot:

![pipeline-page](screenshots/pipeline-page.png)


##### RUN VALIDATION CHECKS INDIVIDUALLY
In order to run individual validation checks at any time, CI environments have
been setup where a button can be clicked in order to start a validation.

For example, let's say the GPU software on the Jupyter worker machine
(w-kirk07-jupyter-0) has just been updated, and I want to check that the
kernels in the HDF5 docker image still work.

1) Go to [Operations > Environments](https://gitlab.maxiv.lu.se/scisw/jupyterhub/jupyter-notebook-validation/-/environments)

![menu-operations-environemnts](screenshots/menu-operations-environemnts.png)

2) Under "Validate Kernels" click the curver arrow icon, which will have "Re-deploy to environment" as its tooltip.  Click the "Re-deploy" botton in the pop-up
window that appears.

![pop-up-re-deploy-environment](screenshots/pop-up-re-deploy-environment.png)

3) Go to [CI/CD > Jobs](https://gitlab.maxiv.lu.se/scisw/jupyterhub/jupyter-notebook-validation/-/jobs)
and see that the job is running, in this case it will have a "Stage" of
"Validate Kernels" and a "Name" of "Docker HDF5":

![jog-list](screenshots/jog-list.png)

4) Click on the "running" status icon, which will take you to the output from
the CI job itself:

![validaiton-results-gitlab-ci](screenshots/validaiton-results-gitlab-ci.png)

There CI jobs setup for running validaiton tests on all kernels and docker
images, as well as for all kernels used in the HPC installation.

Additonally, there are CI jobs setup to pull the newest validation code onto
the docker and HPC installations, as well as to copy the results to a shared
directory:
/mxn/groups/kits/scisw/jupyterhub/jupyter-notebook-validation-test-results/


#### VALIDATION OF CHANGES
The code is this repository is also used in the CI configuration of other
repositories (public gitlab links):
- [jupyter-docker-stacks .gitlab-ci.yml](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jupyter-docker-stacks/-/blob/master/.gitlab-ci.yml)
- [jupyterhub-hpc .gitlab-ci.yml](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jupyterhub-hpc/-/blob/maxiv-setup/.gitlab-ci.yml)
The CI is only setup to be run internally at MAX IV.

The main goal of this project was to be able to run a series of validation
tests on Jupyter kernels as part of a CI pipeline, so that when any kernel or
Jupyter related code changed, tests of the kernels would be made prior to their
deployment onto the produciton JupyterHub service.

To do this, stages were added to the CI pipelines of the repository in which
docker image definitions exist, the resultant images being used in a docker
swarm installation of JupyterHub aswell as a Kubernetes installation of
JupyterHub, and also in the CI pipeline of a repository which defines the
kernels used in the HPC installation.


The stages make use of functions which call the validation routines, for
example:
```bash
.remote_cmd_validate_kernel_template: &remote_cmd_validate_kernel
    REMOTE_CMD: |
        echo "Validate kernel" &&
        cd /home/jupyterhub/jupyter-notebook-validation/ &&
        pwd &&
        make execute-test-compare-slurm \
            PARTITION=$PARTITION \
            KERNEL=$KERNEL \
            NOTEBOOKS=$NOTEBOOKS \
            CONDA_DIR=$CONDA_DIR
```

And the stage which makes use of the function looks like:
```bash
Validate Kernel HDF5:
    stage: Validate Kernels
    only: *hdf5_install_validate_kernel_trigger
    needs: [Install Validation Kernel HDF5]
    variables:
        PARTITION: v100
        KERNEL: maxiv-jhub-hpc-kernel-hdf5_validation
        NOTEBOOKS: notebooks/maxiv/hdf5/*
        CONDA_DIR: /home/jupyterhub/jupyter-aalto/triton-jupyterhub-live/miniconda/
        <<: *remote_cmd_validate_kernel
    <<: *ssh_setup_definition
    <<: *ssh_exec_hpc_kernel_creator_definition
```

Then when a CI stage is triggered, the output can be seen in gitlab (just the
internal gitlab currently, not the public one), for example some dependencies
were changed for a kernel in the HPC installation, which triggered several CI
stages:

![gitlab-output-hpc](screenshots/gitlab-output-hpc.png)

And looking closer at the validation stage, one can check the results:

![gitlab-output-hpc-validate.png](screenshots/gitlab-output-hpc-validate.png)
